
var url = require('url');
var https = require('https');
var SocksProxyAgent = require('socks-proxy-agent');
 
// SOCKS proxy to connect to
var proxy = process.env.socks_proxy || 'socks://103.90.229.148:9999';
console.log('using proxy server %j', proxy);
 
// HTTP endpoint for the proxy to connect to
var endpoint = process.argv[2] || 'https://api.ipify.org?format=json';
console.log('attempting to GET %j', endpoint);
var opts = url.parse(endpoint);
 
// create an instance of the `SocksProxyAgent` class with the proxy server information
// NOTE: the `true` second argument! Means to use TLS encryption on the socket
var agent = new SocksProxyAgent(proxy, true);
opts.agent = agent;
 

 var options = { 
        hostname: 'api.ipify.org',
        path: "/?format=json",
         method: 'GET',
         agent : agent
    };
var request =  https.request(options, (resp) => {
      var data = '';
     
      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk;
      });
     
      // The whole response has been received. Print out the result.
      resp.on('end', () => {
       console.log('end,' + data);
      });
 
}).on("error", (err) => {
  console.log("Error: " + err.message);
});

request.end();